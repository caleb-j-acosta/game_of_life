defmodule Conway.Test do
  use ExUnit.Case
  alias Conway.Grid
  alias Conway.LiveGame

  describe "Conway.Grid" do
    test "new/1 returns a Grid of the given size" do
      grid_size = 5
      grid = Grid.new(grid_size)

      assert grid_size * grid_size ==
               Tuple.to_list(grid.data) |> Enum.map(&Tuple.to_list/1) |> List.flatten() |> length
    end

    test "new/1 returns a grid with the given data" do
      assert Grid.new([[0, 0], [0, 0]]) == %Grid{data: {{0, 0}, {0, 0}}}
    end

    test "new/2 returns a Grid of the given size and probability" do
      assert Grid.new(2, 0) == %Grid{data: {{0, 0}, {0, 0}}}
      assert Grid.new(2, 10) == %Grid{data: {{1, 1}, {1, 1}}}
    end

    test "size/1 returns the size of a Grid" do
      grid = Grid.new(5)
      assert Grid.size(grid) == 5
    end

    test "cell_status/3 returns the state of a cell in the given coordinates" do
      grid = %Grid{data: {{0, 0, 0}, {1, 1, 1}, {1, 0, 1}}}
      assert Grid.cell_status(grid, 0, 0) == 0
    end

    test "next/1 returns the next state of the given grid" do
      grid = %Grid{data: {{1, 0, 1, 0}, {0, 0, 0, 0}, {1, 1, 1, 1}, {1, 0, 0, 0}}}

      assert =
        Grid.next(grid) == %Grid{data: {{0, 0, 0, 0}, {0, 0, 0, 1}, {0, 1, 1, 0}, {0, 1, 1, 0}}}
    end

    test "switch_cell_status/3 returns a grid with the switched selected cell" do
      grid = %Grid{data: {{1, 0, 1}, {0, 0, 0}, {0, 1, 1}}}
      assert Grid.switch_cell_status(grid, 0, 1) == %Grid{data: {{1, 0, 1}, {1, 0, 0}, {0, 1, 1}}}
    end
  end

  describe "Conway.LiveGame" do
    test "print/1 returns the printable list" do
      grid = %Grid{data: {{1, 0}, {0, 1}}}
      assert Conway.LiveGame.print(grid) == [["active-cell", "cell"], ["cell", "active-cell"]]
    end
  end
end
